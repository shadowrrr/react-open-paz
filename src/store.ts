import { configureStore } from '@reduxjs/toolkit'
// import { reducer } from './playlists/containers/PlaylistReducer'
import counterSlice from './reducers/CounterReducer'
import { playlistsSlice } from './reducers/PlaylistsReducer'

export const store = configureStore({
    reducer: {
        playlists: playlistsSlice.reducer,
        counter: counterSlice
    },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch


(window as any).store = store;