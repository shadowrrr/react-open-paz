import React from "react";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { AlbumView } from "../../core/model/Album";

interface Props {
  album: AlbumView;
}

export const AlbumCard = ({ album }: Props) => {
  // const {push,replace} = useHistory()

  return (
    <Link className="card" to={`/music/albums/${album.id}`}>
      <img src={album.images[0].url} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{album.name}</h5>
        {/* <p className="card-text">
            This is a longer card with supporting text below as a natural
            lead-in to additional content. This content is a little bit
            longer.
          </p> */}
      </div>
    </Link>
  );
};
