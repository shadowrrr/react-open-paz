import React, { useEffect, useState } from "react";

interface Props {
  query: string;
  onSearch(query: string): void;
}

export const SearchForm = ({ onSearch, query: parentQuery }: Props) => {
  const [query, setQuery] = useState(parentQuery);

  useEffect(() => {
    setQuery(parentQuery);
  }, [parentQuery]);

  const search = () => onSearch(query);

  return (
    <div className="mb-3">
      <div className="input-group">
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          value={query}
          onKeyUp={(e) => e.key === "Enter" && search()}
          onChange={(e) => setQuery(e.target.value)}
        />
        <button
          className="btn btn-outline-secondary"
          type="button"
          onClick={search}
        >
          Search
        </button>
      </div>
    </div>
  );
};
