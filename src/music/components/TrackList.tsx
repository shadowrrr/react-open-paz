import React, { useEffect, useRef, useState } from "react";
import { Track } from "../../core/model/Album";
import { cls } from "../../playlists/components/utils";

interface Props {
  tracks: Track[];
}

export const TrackList = ({ tracks }: Props) => {
  const [currentTrack, setCurrentTrack] = useState<Track>();
  const [isPlaying, setIsPlaying] = useState(false);
  const audioRef = useRef<HTMLAudioElement>(null);

  const play = (track: Track) => {
    setCurrentTrack(track);
    setIsPlaying(track !== currentTrack || !isPlaying);
  };

  useEffect(() => {
    if (audioRef.current) {
      audioRef.current.volume = 0.05;
      isPlaying ? audioRef.current.play() : audioRef.current.pause();
    }
    return () => {};
  }, [currentTrack, isPlaying]);

  return (
    <div>
      <audio
        src={currentTrack?.preview_url}
        className="w-100 mb-2"
        ref={audioRef}
        controls={true}
      />

      <div className="list-group">
        {tracks.map((track, index) => (
          <div
            className={cls(
              "list-group-item",
              track.id === currentTrack?.id && "active"
            )}
            key={track.id}
            onClick={() => play(track)}
          >
            {index + 1}. {track.name}
          </div>
        ))}
      </div>
    </div>
  );
};
