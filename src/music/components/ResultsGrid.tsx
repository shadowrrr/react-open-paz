import React from "react";
import { AlbumView } from "../../core/model/Album";
import { AlbumCard } from "./AlbumCard";

interface Props {
  results: AlbumView[];
}

export const ResultsGrid = ({ results }: Props) => {
  return (
    <div>
      <div className="row row-cols-1 row-cols-sm-4 g-0">
        {results.map((result) => (
          <div className="col" key={result.id}>
            <AlbumCard album={result}/>
          </div>
        ))}
      </div>
    </div>
  );
};
