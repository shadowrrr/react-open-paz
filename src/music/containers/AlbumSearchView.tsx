import { useCallback, useEffect, useState } from "react";
import { ResultsGrid } from "../components/ResultsGrid";
import { SearchForm } from "../components/SearchForm";
import { useSearchAlbums } from "../../core/hooks/useSearchAlbums";
import { RouteComponentProps, useHistory, useLocation } from "react-router-dom";

// interface Props extends RouteComponentProps<any> {}
interface Props {}

export const AlbumSearchView = ({}: Props) => {
  const [query, setQuery] = useState("batman");
  const { results, error, loading } = useSearchAlbums(query);

  const location = useLocation();
    useEffect(() => {
    const q = new URLSearchParams(location.search).get("q");
    q && setQuery(q);
  }, [location.search]);
  
  const history = useHistory();
  const search = useCallback(
    (query) => history.push("/music/search?q=" + query),
    []
  );

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm onSearch={search} query={query} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {loading && <p className="alert alert-info mb-3">Loading ...</p>}
          {error && <p className="alert alert-danger mb-3">{error.message}</p>}

          <ResultsGrid results={results} />
        </div>
      </div>
    </div>
  );
};
