import { createAsyncThunk, createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { stat } from "fs";
import { PagingObject } from "../core/model/Album";
import { Playlist } from "../core/model/Playlist";
import { RootState } from "../store";


interface PlaylistsState {
    items: Playlist[];
    selectedId?: Playlist["id"];
    mode: "details" | "editor" | "create";
    loading: boolean;
    message: string;
}

export const initialState: PlaylistsState = {
    items: [],
    selectedId: undefined,
    mode: "details",
    loading: false,
    message: "",
};



export const fetchPlaylists = createAsyncThunk(
    'playlists/fetchByIdStatus',
    async (thunkAPI) => {
        await new Promise(r => setTimeout(r, 100));
        const res = await axios
            .get<PagingObject<Playlist>>("https://api.spotify.com/v1/me/playlists")

        return { data: res.data.items }
    }
)

export const playlistsSlice = createSlice({
    name: 'playlists',
    initialState,
    reducers: {
        playlistSelect: (state, action: PayloadAction<Playlist['id']>) => {
            state.selectedId = action.payload
        },
        playlistRemove: (state, action: PayloadAction<Playlist['id']>) => {
            state.items = state.items.filter(p => p.id !== action.payload)
        },
        playlistUpdate: (state, action: PayloadAction<Playlist>) => {
            const draft = action.payload
            state.items = state.items.map(p => p.id === draft.id ? draft : p)
        },
        playlistCreate: (state, action: PayloadAction<Playlist>) => {
            const draft = action.payload
            state.items.push(draft)
        },
        createMode(state) { state.mode = 'create' },
        cancel(state) { state.mode = 'details' },
        edit(state) { state.mode = 'editor' },
    },
    extraReducers(builder) {
        builder.addCase(fetchPlaylists.pending, (state, action) => {
            state.items = []
            state.message = ''
            state.loading = true
        })
        builder.addCase(fetchPlaylists.fulfilled, (state, action) => {
            state.items = action.payload.data
            state.loading = false
        })
        builder.addCase(fetchPlaylists.rejected, (state, action) => {
            state.message = action.error.message || ''
        })
    }
})

export const {
    createMode,
    cancel,
    edit, playlistSelect, playlistRemove, playlistUpdate,
    playlistCreate } = playlistsSlice.actions


const selectFeature = createSelector((state: RootState) => state, state => state.playlists)
export const selectPlaylists = createSelector(selectFeature, state => state.items)
export const selectCurrentPlaylist = createSelector(
    selectPlaylists,
    createSelector(selectFeature, state => state.selectedId),
    (items, selectedId) => items.find(p => p.id === selectedId))

export const playlistsUI = createSelector(selectFeature, ({
    loading, message, mode
}) => ({
    loading, message, mode
}))
