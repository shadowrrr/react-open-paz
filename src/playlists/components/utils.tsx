// https://www.npmjs.com/package/classnames
const utils = {};

export const cls = (...classes: (string | false)[]) => classes.filter(Boolean).join(" ");
