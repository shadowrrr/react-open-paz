// tsrafc
import React from "react";
import { Playlist } from "../../core/model/Playlist";

// import PropTypes from "prop-types";

interface Props {
  playlist?: Playlist;
  onEdit: () => void
}

// export const PlaylistDetails = ({ playlist}: Props = {playlist:{}}) => {
export const PlaylistDetails = React.memo( ({ playlist, onEdit }: Props) => {
  // const playlist = props.playlist;
  // const { playlist } = props;

  if (!playlist) return <p>No playlist selected {playlist}</p>;

  return (
    <div>
      <dl
        id={"playlist_" + playlist.id}
        data-playlist-id={playlist.id}
        title={playlist.name}
      >
        <dt>Name:</dt>
        <dd>{playlist.name}</dd>

        <dt>Public:</dt>
        <dd style={{ color: playlist.public ? "red" : "green" }}>
          {playlist.public ? "Yes" : "No"}
        </dd>

        <dt>Description:</dt>
        <dd>{playlist.description}</dd>
      </dl>

      <button className="btn btn-info" onClick={onEdit}>
        Edit
      </button>
    </div>
  );
})

// https://pl.reactjs.org/docs/typechecking-with-proptypes.html
// Warning: Failed prop type: The prop `playlist` is marked as required in `PlaylistDetails`, but its value is `undefined`.
// Warning: Failed prop type: The prop `playlist.id` is marked as required in `PlaylistDetails`, but its value is `undefined`.
// PlaylistDetails.propTypes = {
//   playlist: PropTypes.shape({
//     id: PropTypes.string.isRequired,
//     name: PropTypes.string.isRequired,
//     public: PropTypes.bool.isRequired,
//     description: PropTypes.string.isRequired,
//   }).isRequired
// };
// PlaylistDetails.defaultProps = { playlist: {} };
