import { useCallback, useEffect, useMemo, useReducer } from "react";
import { useDispatch, useSelector, useStore } from "react-redux";
import { bindActionCreators } from "redux";
import {
  cancel,
  createMode,
  edit,
  fetchPlaylists,
  playlistCreate,
  playlistRemove,
  playlistSelect,
  playlistsUI,
  playlistUpdate,
  selectCurrentPlaylist,
  selectPlaylists,
} from "../../reducers/PlaylistsReducer";
import { AppDispatch, RootState } from "../../store";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistEditor } from "../components/PlaylistEditor";
import { PlaylistList } from "../components/PlaylistList";

/* ================ */

interface Props {}
export const PlaylistsReducerView = (props: Props) => {
  // useStore()
  // const state = useSelector<RootState>((state) => state.playlists.items);
  const dispatch = useDispatch<AppDispatch>();

  const playlists = useSelector(selectPlaylists);
  const selected = useSelector(selectCurrentPlaylist);
  const { loading, message, mode } = useSelector(playlistsUI);

  useEffect(() => {
    dispatch(fetchPlaylists());
  }, []);

  const actions = useMemo(
    () =>
      bindActionCreators(
        {
          playlistSelect,
          playlistRemove,
          createMode,
          playlistUpdate,
          playlistCreate,
          cancel,
          edit,
        },
        dispatch
      ),
    []
  );

  return (
    <div>
      <div className="row">
        <div className="col">
          {loading && <p className="alert alert-info">Loading</p>}
          {message && <p className="alert alert-info">{message}</p>}
        </div>
      </div>
      <div className="row">
        <div className="col">
          <PlaylistList
            playlists={playlists}
            selectedId={selected?.id}
            onSelect={actions.playlistSelect}
            onRemove={actions.playlistRemove}
          />
          <button
            className="btn btn-info float-end mt-3"
            onClick={actions.createMode}
          >
            Create New
          </button>
        </div>
        <div className="col">
          {selected && mode === "details" && (
            <PlaylistDetails
              playlist={selected}
              onEdit={() => actions.edit()}
            />
          )}

          {selected && mode === "editor" && (
            <PlaylistEditor
              playlist={selected}
              onCancel={actions.cancel}
              onSave={actions.playlistUpdate}
            />
          )}

          {mode === "create" && (
            <PlaylistEditor
              onCancel={() => actions.cancel()}
              onSave={actions.playlistCreate}
            />
          )}
        </div>
      </div>
    </div>
  );
};
