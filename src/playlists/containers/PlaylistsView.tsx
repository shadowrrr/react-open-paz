import { useCallback, useEffect, useMemo, useState } from "react";
import { Playlist } from "../../core/model/Playlist";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistEditor } from "../components/PlaylistEditor";
import { PlaylistList } from "../components/PlaylistList";
import { playlistsMock } from "./playlistsMock";

interface Props {}
export const PlaylistsView = (props: Props) => {
  const [playlists, setPlaylists] = useState(playlistsMock);
  const [selectedId, setSelectedId] = useState<Playlist["id"] | undefined>();
  const [selected, setSelected] = useState<Playlist | undefined>();
  const [mode, setMode] = useState<"details" | "editor" | "create">("details");

  useEffect(() => selectPlaylistById("123"), []);
  useEffect(
    () => setSelected(() => playlists.find((p) => p.id === selectedId)),
    [selectedId, playlists]
  );

  const selectPlaylistById = useCallback((id: string) => {
    setSelectedId((selectedId) => (selectedId === id ? undefined : id));
  }, []);

  const cancel = useCallback(() => setMode("details"), []);
  const edit = useCallback(() => setMode("editor"), []);
  const saveChangedPlaylist = useCallback((draft: Playlist) => {
    setPlaylists((playlists) =>
      playlists.map((p) => (p.id === draft.id ? draft : p))
    );
    setSelectedId(draft.id);
    setMode("details");
  }, []);

  const removePlaylistById = useCallback((id: Playlist["id"]) => {
    setPlaylists((playlists) => playlists.filter((p) => p.id !== id));
    // setMode("details");
  }, []);

  const createMode = () => {
    setMode("create");
    setSelectedId(undefined);
  };

  const saveNewPlaylist = useCallback((draft: Playlist) => {
    draft.id = Date.now().toString();
    setPlaylists((playlists) => [...playlists, draft]);
    setSelectedId(draft.id);
    setMode("details");
  }, []);

  const emptyPlaylist = useMemo(
    () => ({
      id: "",
      name: "",
      public: false,
      description: "",
    }),
    []
  );

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistList
            playlists={playlists}
            selectedId={selectedId}
            onSelect={selectPlaylistById}
            onRemove={removePlaylistById}
          />
          <button className="btn btn-info float-end mt-3" onClick={createMode}>
            Create New
          </button>
        </div>
        <div className="col">
          {selected && mode === "details" && (
            <PlaylistDetails playlist={selected} onEdit={edit} />
          )}

          {selected && mode === "editor" && (
            <PlaylistEditor
              playlist={selected}
              onCancel={cancel}
              onSave={saveChangedPlaylist}
            />
          )}
          {mode === "create" && (
            <PlaylistEditor
              playlist={emptyPlaylist}
              onCancel={cancel}
              onSave={saveNewPlaylist}
            />
          )}
        </div>
      </div>
    </div>
  );
};
