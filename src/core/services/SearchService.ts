import axios, { AxiosResponse } from "axios"
import {  AlbumsSearchResponse } from "../model/Album"

export const fetchSearchResults = (query: string) => {
    return axios.get<unknown>(`https://api.spotify.com/v1/search`, {
        params: {
            type: 'album',
            q: query
        }
    })
        .then(resp => {
            validateAlbumsSearch(resp)
            return resp.data.albums.items
        })

}

function validateAlbumsSearch(resp: any): asserts resp is AxiosResponse<AlbumsSearchResponse> {
    if (!('albums' in resp.data && 'items' && resp.data.albums)) {
        throw Error('Invalid album data')
    }
}