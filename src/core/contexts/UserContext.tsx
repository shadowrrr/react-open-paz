import React, { FC } from "react";
import { UserProfile } from "../model/UserProfile";

interface UserCtx {
  user: UserProfile | null;
  login: () => void;
  logout: () => void;
}
export interface SpotifyErrorResponse {
  error: { message: string; status: number };
}

export const userContext = React.createContext<UserCtx>({
  user: null,
} as UserCtx);
