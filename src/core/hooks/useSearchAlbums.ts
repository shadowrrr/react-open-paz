import useSWR from "swr";
import { AlbumsSearchResponse } from "../../core/model/Album";

//   const { results, error, loading } = useSearchAlbums(query);


export function useSearchAlbums(query: string) {
    const {
        data,
        error,
        isValidating: loading,
    } = useSWR<AlbumsSearchResponse>(
        query && `https://api.spotify.com/v1/search?type=album&q=${query}`
    );
    const results = data?.albums.items || [];

    return { results, error, loading }
}



