import useSWR from "swr";
import { Album } from "../../core/model/Album";



export function useFetchAlbum(id: string) {
    const {
        data, error, isValidating: loading,
    } = useSWR<Album>(
        id && `https://api.spotify.com/v1/albums/${id}`
    );

    return { data, error, loading };
}
