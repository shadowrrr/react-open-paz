import { useEffect, useState } from "react";

  // const {
  //   results = [],
  //   message,
  //   loading,
  // } = useFetch(fetchSearchResults, query);


export function useFetch<TQ, TR>(fetcher: (query: TQ) => Promise<TR>, query: TQ) {
  const [results, setResults] = useState<TR | undefined>(undefined);
  const [message, setMessage] = useState("");

  useEffect(() => {
    if (!query)
      return;

    setResults(undefined);
    setMessage("");

    fetcher(query)
      .then((results) => setResults(results))
      .catch((error) => setMessage(error.message));
  }, [query, fetcher]);

  return { results, message, loading: !message && !results };
}
