import { useContext } from "react";
import { userContext } from "../contexts/UserContext";


export const useUser = () => {
  const { user } = useContext(userContext);
  return user;
};
