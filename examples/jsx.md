```tsx

window.React = React;
window.ReactDOM = ReactDOM;
interface User {
  id: string;
  name: string;
  color: string;
  pet: string;
}

// const user: User = { id: "123", name: "User", color: "red", pet: "Cat" };
const users: User[] = [
  { id: "345", name: "Cat", color: "blue", pet: "Fish" },
  { id: "234", name: "Bob", color: "green", pet: "Dog" },
  { id: "123", name: "Alice ", color: "red", pet: "Cat" },
];

// const Person = (props: { user: User }) =>
//   React.createElement(
//     "div",
//     { id: props.user.id, className: "person-info" },
//     props.user.name + " has a  ",
//     React.createElement(
//       "b",
//       { style: { color: props.user.color } },
//       props.user.pet
//     )
//   );

/* Functional component */
const Person = (props: { user: User }) => (
  <div className="person-info" id={props.user.id}>
    {props.user.name} has a
    <b style={{ color: props.user.color }}> {props.user.pet}</b>
  </div>
);

debugger

// const UsersList = (props: { users: User[] }) =>
//   React.createElement(
//     "ul",
//     {},
//     props.users.map((user) =>
//       React.createElement("li", { key: user.id }, Person({ user }))
//     )
//   );
const UsersList = (props: { users: User[] }) => (
  <ul>
    {props.users.map((user) => (
      // <li key={user.id}>{Person({ user: user })}</li>
      <li key={user.id}>
        <Person user={user} />
      </li>
    ))}
  </ul>
);

// ReactDOM.render(UsersList({ users: users }), document.getElementById("root"));
ReactDOM.render(
  <div>
    <UsersList users={users}></UsersList>
  </div>,
  document.getElementById("root")
);


```