
```js

inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) =>  ({type:'DEC', payload});
addTodo = (payload='newTodo') =>  ({type:'ADD_TODO', payload});

[inc(),inc(2),addTodo('buy milk!'), inc(1),dec(3)].reduce( (state, action)=>{
    console.log(state, action)
    switch(action.type){
        case 'INC': return {
            ...state, counter: state.counter + action.payload
        }   
        case 'DEC': return {
            ...state, counter: state.counter - action.payload
        }  
        case 'ADD_TODO': return {
            ...state, todos: [ ...state.todos, action.payload ]
        }    
        default: return state
    }
},{
    counter: 0,
    todos: []
})
// {counter: 0, todos: Array(0)} {type: 'INC', payload: 1}
// {counter: 1, todos: Array(0)} {type: 'INC', payload: 2}
// {counter: 3, todos: Array(0)} {type: 'ADD_TODO', payload: 'buy milk!'}
// {counter: 3, todos: Array(1)} {type: 'INC', payload: 1}
// {counter: 4, todos: Array(1)} {type: 'DEC', payload: 3}
// {counter: 1, todos: Array(1)}

```

```ts

inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) =>  ({type:'DEC', payload});
addTodo = (payload='newTodo') =>  ({type:'ADD_TODO', payload});

reducer = (state, action)=>{
    console.log(state, action)
    switch(action.type){
        case 'INC': return {
            ...state, counter: state.counter + action.payload
        }   
        case 'DEC': return {
            ...state, counter: state.counter - action.payload
        }  
        case 'ADD_TODO': return {
            ...state, todos: [ ...state.todos, action.payload ]
        }    
        default: return state
    }
}
state = {
    counter: 0,
    todos: []
}


state = reducer(state, inc(2))
state = reducer(state, dec(1))
state = reducer(state, addTodo('buy milk!'))


{counter: 0, todos: Array(0)} {type: 'INC', payload: 2}
{counter: 2, todos: Array(0)} {type: 'DEC', payload: 1}
{counter: 1, todos: Array(0)} {type: 'ADD_TODO', payload: 'buy milk!'}
{counter: 1, todos: Array(1)}

```


```ts
inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) =>  ({type:'DEC', payload});
addTodo = (payload='newTodo') =>  ({type:'ADD_TODO', payload});

counter = (state = 0, action) => {
    switch(action.type){
      case 'INC': return  state + action.payload            
      case 'DEC': return   state - action.payload
      default: return state         
    }
}

reducer = (state, action)=>{
    console.log(state, action)
    switch(action.type){       
        case 'ADD_TODO': return {
            ...state, todos: [ ...state.todos, action.payload ]
        }    
        default: return {
            ...state, 
            counter: counter(state.counter, action)
             /*, red2(...), red3(...) */ 
        }
    }
}
state = {
    counter: 0,
    todos: []
}


state = reducer(state, inc(2))
state = reducer(state, dec(1))
state = reducer(state, addTodo('buy milk!'))


{counter: 0, todos: Array(0)} {type: 'INC', payload: 2}
{counter: 2, todos: Array(0)} {type: 'DEC', payload: 1}
{counter: 1, todos: Array(0)} {type: 'ADD_TODO', payload: 'buy milk!'}
{counter: 1, todos: Array(1)}

```